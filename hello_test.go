package main

import "testing"

func TestHello(t *testing.T) {
	assertMessage := func (t testing.TB, got, want string) {
		t.Helper()
		if got != want {
			t.Errorf("got %q want %q", got, want)
		}
	}

	t.Run("Happy path", func (t *testing.T) {
		got := Hello("Camilo")
		want := "Hello, Camilo"

		assertMessage(t, got, want)
	})

	t.Run("Negative path", func (t *testing.T) {
		got := Hello("")
		want := "Hello, world"

		assertMessage(t, got, want)
	})

	t.Run("passing a number", func (t *testing.T) {
		got := Hello("1")
		want := "Hello, 1"

		assertMessage(t, got, want)
	})
}

func TestHelloTable(t *testing.T) {
	tableTests := []struct{
		name string
		want string
	}{
		{name: "Camilo", want: "Hello, Camilo"},
		{name: "", want: "Hello, world"},
		{name: "1", want: "Hello, 1"},
	}

	for _, tt := range tableTests {
		got := Hello(tt.name)
		if got != tt.want {
			t.Errorf("got %q want %q", got, tt.want)
		}
	}
}